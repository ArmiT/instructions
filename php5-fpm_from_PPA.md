Install PHP5.5-fpm from PPA
======
PPA лежит здесь
https://launchpad.net/~ondrej/+archive/php5

Пытаемся добавить репозиторий
```sh
sudo add-apt-repository ppa:ondrej/php5
```
Если система говорит, что команда add-apt-repository неизвестна, то
устанавливаем ее
```sh
apt-get install python-software-properties
```
После чего повторяем 
```sh
sudo add-apt-repository ppa:ondrej/php5
```
И обновляем данные по пакетам
```sh
sudo apt-get update
```
в директории /etc/apt/sources.list.d/ должен появиться файл
ondrej-php5-precise.list со следующим содержимым (для Ubuntu 12.04)
```sh
eb http://ppa.launchpad.net/ondrej/php5/ubuntu precise main
deb-src http://ppa.launchpad.net/ondrej/php5/ubuntu precise main
```
Затем добавляем ключ PPA в систему. Ключ это часть строки Signing key после слеша на странице PPA (в данном случае https://launchpad.net/~ondrej/+archive/php5)

Signing key для этого PPA
```SH
1024R/E5267A6C
```
А ключ
```SH
E5267A6C
```
Импортируем ключ
```sh
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E5267A6C
```
И обновляем данные по пакетам
```sh
sudo apt-get update
sudo apt-get dist-upgrade
```
После чего можно ставить php
```sh
sudo apt-get install php5-fpm
```
Дополнительные пакеты ставятся отдельно, например
```sh
apt-get install php5-mysql
apt-get install php5-gd
apt-get install php5-json
```
