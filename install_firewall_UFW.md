Install Ubuntu simple firewall UFW
===
Установка
```sh
sudo apt-get install ufw
```
Добавление правил:

Закрыть порт 8080
```sh
ufw deny 8080/tcp
```

Открыть порт 22
```sh
ufw allow 22/tcp
```
Применить
```sh
ufw enable
```

Получить статус
```sh
ufw status numbered
```

