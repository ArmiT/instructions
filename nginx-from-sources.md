Install nginX from sources
=====
Обновляем данные о пакетах
```sh
apt-get update
```
Затем, устанавливаем зависимости
```sh
aptitude -y install build-essential libc6 libpcre3 libpcre3-dev libpcrecpp0 libssl0.9.8 libssl-dev zlib1g zlib1g-dev lsb-base
```
Идем на [офф. сайт](http://nginx.org/en/download.html) Nginx и забираем ссылку на последний стабильный релиз.
Например http://nginx.org/download/nginx-1.4.6.tar.gz

Скачиваем архив с исходниками в директорию /usr/src
```sh
wget http://nginx.org/download/nginx-1.4.6.tar.gz -P /usr/src
```
Распаковываем
```sh
cd /usr/src
tar -xvf nginx-1.4.6.tar.gz
cd nginx-1.4.6
```
Конфигурация
```sh
./configure --sbin-path=/usr/sbin --conf-path=/etc/nginx/nginx.conf --error-log-path=/var/log/nginx/error.log --pid-path=/var/run/nginx.pid --lock-path=/var/lock/nginx.lock --http-log-path=/var/log/nginx/access.log --http-client-body-temp-path=/var/lib/nginx/body --http-proxy-temp-path=/var/lib/nginx/proxy --http-fastcgi-temp-path=/var/lib/nginx/fastcgi --with-debug --with-http_stub_status_module --with-http_flv_module --with-http_ssl_module --with-http_dav_module --with-ipv6
```
```sh
make 
sudo make install
```
Если все скомпилилось без проблем, можем проверить версию
```sh
nginx -v
```
Должно вернуться что-то вроде этого
```sh
nginx version: nginx/1.4.6
```
Осталось добавить файл /etc/init.d/nginx
```sh
#! /bin/sh

### BEGIN INIT INFO
# Provides:          nginx
# Required-Start:    $all
# Required-Stop:     $all
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: starts the nginx web server
# Description:       starts nginx using start-stop-daemon
### END INIT INFO

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
DAEMON=/usr/sbin/nginx
NAME=nginx
DESC=nginx

test -x $DAEMON || exit 0

# Include nginx defaults if available
if [ -f /etc/default/nginx ] ; then
        . /etc/default/nginx
fi

set -e

case "$1" in
  start)
        echo -n "Starting $DESC: "
        start-stop-daemon --start --quiet --pidfile /var/run/$NAME.pid --exec $DAEMON -- $DAEMON_OPTS || true
        echo "$NAME."
        ;;
  stop)
        echo -n "Stopping $DESC: "
        start-stop-daemon --stop --quiet --pidfile /var/run/$NAME.pid --exec $DAEMON || true
        echo "$NAME."
        ;;
  restart|force-reload)
        echo -n "Restarting $DESC: "
        start-stop-daemon --stop --quiet --pidfile /var/run/$NAME.pid --exec $DAEMON || true
        sleep 1
        start-stop-daemon --start --quiet --pidfile /var/run/$NAME.pid --exec $DAEMON -- $DAEMON_OPTS || true
        echo "$NAME."
        ;;
  reload)
        echo -n "Reloading $DESC configuration: "
        start-stop-daemon --stop --signal HUP --quiet --pidfile /var/run/$NAME.pid --exec $DAEMON || true
        echo "$NAME."
        ;;
  configtest)
        echo -n "Testing $DESC configuration: "
        if nginx -t > /dev/null 2>&1
        then
          echo "$NAME."
        else
          exit $?
        fi
        ;;
  *)
        echo "Usage: $NAME {start|stop|restart|reload|force-reload|configtest}" >&2
        exit 1
        ;;
esac

exit 0
```

Дать права на выполнение
```sh
chmod +x /etc/init.d/nginx
```
И прописать в boot
```sh
update-rc.d nginx defaults
```