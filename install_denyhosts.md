Install DenyHosts
===
Устанавливаем
```sh
sudo apt-get install denyhosts
```

Пример конфига (только измененные значения)
```sh
SECURE_LOG = /var/log/messages
HOSTS_DENY = /etc/hosts.deny
PURGE_DENY = 
BLOCK_SERVICE = ALL
DENY_THRESHOLD_INVALID = 5
DENY_THRESHOLD_VALID = 5
DENY_THRESHOLD_ROOT = 3
DENY_THRESHOLD_RESTRICTED = 1
WORK_DIR = /var/lib/denyhosts
SUSPICIOUS_LOGIN_REPORT_ALLOWED_HOSTS=YES
HOSTNAME_LOOKUP=YES
LOCK_FILE = /var/run/denyhosts.pid
ADMIN_EMAIL = admin@mail.com
SMTP_HOST = localhost
SMTP_PORT = 25
SMTP_FROM = DenyHosts <nobody@localhost>
SMTP_SUBJECT = DenyHosts Report
SYSLOG_REPORT=YES
AGE_RESET_VALID=5d
AGE_RESET_ROOT=25d
AGE_RESET_RESTRICTED=25d
AGE_RESET_INVALID=10d
DAEMON_LOG = /var/log/denyhosts
 
DAEMON_SLEEP = 30s
DAEMON_PURGE = 1h
SYNC_SERVER = http://xmlrpc.denyhosts.net:9911
```

Рестарт демона
```sh
sudo /etc/init.d/denyhosts restart
```                                                                                 